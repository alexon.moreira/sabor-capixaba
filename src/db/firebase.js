//import * as firebase from 'firebase';
import Firebase from 'firebase/app';
import 'firebase/database';

const config = {
    apiKey: "AIzaSyBGF13VxhcbJzta3BxZ8D58YXDf4rQBzPE",
    authDomain: "sabor-capixaba.firebaseapp.com",
    databaseURL: "https://sabor-capixaba.firebaseio.com",
    projectId: "sabor-capixaba",
    storageBucket: "sabor-capixaba.appspot.com",
    messagingSenderId: "956896540524"    
  };
Firebase.initializeApp(config);
/*
if(!firebase.apps.length) {
    firebase.initializeApp(config);
} 
*/ 

export const firebase = Firebase.database()