import {combineReducers} from 'redux';
import reducerApp from './actions';
import reducerEnderecoAtivo from './actions';
import reducerMenuLeft from './actions';
import reducerSnackbar from './actions';
import reducerCategoria from './actions';
import reducerContadorInc from './actions';
import reducerContadorDec from './actions';

export default combineReducers({
    reducerApp,
    reducerEnderecoAtivo,
    reducerMenuLeft,
    reducerSnackbar,
    reducerCategoria,
    reducerContadorInc,
    reducerContadorDec,
});

