import {createActions, createReducer} from "reduxsauce";

export const {Types, Creators} = createActions({
    actionApp: ['payload'],
    actionEnderecoAtivo: ['payload'],
    actionMenuLeft: ['payload'],
    actionCategoria: ['payload'],
    /** Contador Actons  */
    actionContadorInc: ['payload'],
    actionContadorDec: ['payload']
});

const STATES = {
    /* states props App */
    propsApp_OPACITY:1,
    
    /* states props MenuLeft */
    propsMenuLeft_WIDTH:0,
    propsMenuLeft_SIZEIMAGE:0,
    propsMenuLeft_STATUS_MENU_LEFT:false,
    
    /* states props EnderecoAtivo */    
    propsEnderecoAtivo_CEP:'', 
    propsEnderecoAtivo_LOGRADOURO:'', 
    propsEnderecoAtivo_ENDNUM:'', 
    propsEnderecoAtivo_BAIRRO:'', 
    propsEnderecoAtivo_MUNICIPIO:'', 
    propsEnderecoAtivo_ESTADO:'', 
    propsEnderecoAtivo_PAIS:'',
    propsEnderecoAtivo_ENDATIVO:false,
    propsEnderecoAtivo_REDIRECT:false,    
    
    /* states props Categoria */
    propsCategoria_ID:'',
    propsCategoria_DESCRICAO:'',    

    /* states props Contador */
    propsContador_NUM:0
}

/************************ APP ************************/
const reducerApp = (state=STATES, action) => {
    return {
        ...state,
        propsApp_OPACITY: state.stateApp_OPACITY = action.payload.opacity,
    }
};

/************************ MENU LEFT ************************/

const reducerMenuLeft = (state=STATES, action) => {
    return {
        ...state,
       propsMenuLeft_WIDTH:            state.stateMenuLeft_WIDTH               = action.payload.width,
       propsMenuLeft_SIZEIMAGE:        state.stateMenuLeft_SIZEIMAGE           = action.payload.sizeImage,
       propsMenuLeft_STATUS_MENU_LEFT:!state.propsMenuLeft_STATUS_MENU_LEFT
    }
};

/************************ ENDERECO ATIVO  ************************/
const reducerEnderecoAtivo = (state=STATES, action) => {
    return {
        ...state,
        propsEnderecoAtivo_ENDATIVO:   state.stateEnderecoAtivo_ENDATIVO     = action.payload.endAtivo,
        propsEnderecoAtivo_REDIRECT:   state.stateEnderecoAtivo_REDIRECT     = action.payload.redirect,
        propsEnderecoAtivo_CEP:        state.stateEnderecoAtivo_CEP          = action.payload.cep,
        propsEnderecoAtivo_LOGRADOURO: state.stateEnderecoAtivo_LOGRADOURO   = action.payload.logradouro,
        propsEnderecoAtivo_ENDNUM:     state.stateEnderecoAtivo_ENDNUM       = action.payload.endNum,
        propsEnderecoAtivo_BAIRRO:     state.stateEnderecoAtivo_BAIRRO       = action.payload.bairro,
        propsEnderecoAtivo_MUNICIPIO:  state.stateEnderecoAtivo_MUNICIPIO    = action.payload.municipio,
        propsEnderecoAtivo_ESTADO:     state.stateEnderecoAtivo_ESTADO       = action.payload.estado,
        propsEnderecoAtivo_PAIS:       state.stateEnderecoAtivo_PAIS         = action.payload.pais
    }
};

/************************ CATEGORIA ************************/

const reducerCategoria = (state=STATES, action) => {
    return {
        ...state,
       propsCategoria_ID:         state.stateCategoria_ID          = action.payload.id,
       propsCategoria_DESCRICAO:  state.stateCategoria_DESCRICAO   = action.payload.descricao
    }
};

/************************ CONTADOR ************************/

const reducerContadorInc = (state=STATES) => {
    
    return {
        ...state,
        propsContador_NUM:state.propsContador_NUM+1
    }
};

const reducerContadorDec = (state=STATES) => {
    return {        
        ...state,
        propsContador_NUM:state.propsContador_NUM-1
    }
};

export default createReducer(STATES, {
    [Types.ACTION_APP]:reducerApp,
    [Types.ACTION_ENDERECO_ATIVO]:reducerEnderecoAtivo,
    [Types.ACTION_MENU_LEFT]:reducerMenuLeft,
    [Types.ACTION_CATEGORIA]:reducerCategoria,
    /** Contador Reducers  */
    [Types.ACTION_CONTADOR_INC]:reducerContadorInc,
    [Types.ACTION_CONTADOR_DEC]:reducerContadorDec,
});