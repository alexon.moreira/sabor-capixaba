import React, { Component } from 'react';
import {Image} from 'react-native';
import '../css/background-image.css';

class BackgroundImage extends Component {
    render() {
        return (
            <Image className='fullsize-image-bg' source={require(`../images/saborcapixaba/${this.props.backgroundImage}`)}></Image>
        )
    }
}
export default BackgroundImage