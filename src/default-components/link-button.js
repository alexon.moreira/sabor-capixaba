import React, {Component} from 'react';
import Icon from 'react-native-vector-icons/dist/MaterialIcons';
import {Text, View, TouchableHighlight} from 'react-native';
import {Link as NativeLink} from 'react-router-native';
import '../css/button3d.css';

class LinkButton extends Component {
    render() {
      const {onClick, children} = this.props;

        return (
            <NativeLink className="link-button" to={this.props.to}>
                <TouchableHighlight style={[{...this.props.style}]} onPress={onClick} underlayColor={'transparent'}>
                    <View className='button3d-container' style={[{...this.props.style}]}>
                        <Icon className='button3d-icon' name={this.props.icon} size={30} />
                        <Text className='button3d-texto' style={[{...this.props.style}]}>{children}</Text>
                    </View>
                </TouchableHighlight>
            </NativeLink>                
        );
    }    
}

export default LinkButton;
