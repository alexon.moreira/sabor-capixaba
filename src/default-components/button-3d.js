import React, {Component} from 'react';
import {View, Text, TouchableHighlight} from 'react-native';
import Icon from 'react-native-vector-icons/dist/MaterialIcons';
import '../css/button3d.css';

class Button3D extends Component {
  render() {
    const {onClick, children} = this.props;

    return (
      <TouchableHighlight style={[{...this.props.style, margin:1, marginTop:10}]} onPress={onClick}>
          <View className='button3d-container' style={[{...this.props.style}]}>
            <Icon className='button3d-icon' name={this.props.icon} size={30} />
            <Text className='button3d-text' adjustsFontSizeToFit>{children}</Text>
          </View>            
      </TouchableHighlight>
    );
  }
}

export default Button3D;
