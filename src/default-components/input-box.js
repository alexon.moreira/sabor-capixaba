import React, {Component} from 'react';
import {View, TextInput, StyleSheet} from 'react-native';
import {shadow} from './helpers';
import '../css/input-box.css';

class InputBox extends Component {
  render() {
      const {autoFocus, onChangeText, onChange, value, placeholder, name, secureTextEntry} = this.props
    return (
        <View className='container-input-box' style={{width:this.props.widthBox}}>            
            <TextInput secureTextEntry={secureTextEntry} className='input-box' name={name} value={value} onChangeText={onChangeText} onChange={onChange} style={[styles.inputBox, this.props.style]} placeholder={placeholder} autoFocus={autoFocus}/>
        </View>
    );
  }
}

const styles = StyleSheet.create({  
  inputBox: {...shadow, height:'100%'},
  labelBox: {color:'#fff'}
});

export default InputBox;
