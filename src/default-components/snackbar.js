import React, {Component} from 'react'; 
import {Animated, Text} from 'react-native';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {Creators as Actions} from '../redux/actions';
import '../css/snackbar.css';
 
class Snackbar extends Component {

   render() {       
       console.log(this.props.message);
        return( 
            <Animated.View> 
                <Text className='snackbar-text' style={[{color:'black'}]} numberOfLines={2}>{this.props.message}</Text>
            </Animated.View>         
        );
    }
}

const mapStateToProps = state => ({...state});
const mapDispatchToProps = dispatch => bindActionCreators(Actions, dispatch);
export default connect(mapStateToProps, mapDispatchToProps)(Snackbar);


