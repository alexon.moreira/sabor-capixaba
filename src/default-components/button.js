import React, {Component} from 'react';
import {StyleSheet, Text, TouchableHighlight} from 'react-native';
import {shadow} from './helpers';
import '../css/button.css';

class Button extends Component {
  render() {
    const {onClick, children} = this.props;

    return (      
      <TouchableHighlight className='button' style={[styles.button, this.props.style]} onPress={onClick} underlayColor={'transparent'}>
        <Text className='text-button' style={[{color:this.props.color}]}>{children}</Text>
      </TouchableHighlight>
    );
  }
}

const styles = StyleSheet.create({  
  button: {...shadow, marginVertical:10}
});

export default Button;