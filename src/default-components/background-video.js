import React, {Component} from 'react';
import {View} from 'react-native';
import Video8 from '../videos/small/v8.mp4';
import '../css/background-video.css';

class BackgroundVideo extends Component {
  state = {stateVideo8: Video8}

  render() {
    return (
      <View className='fullsize-video-bg'>  
		    <video loop autoPlay muted>      
			    <source src={this.state.stateVideo8} type="video/mp4" />
		    </video>  
      </View>      
    );
  }
}

export default BackgroundVideo;