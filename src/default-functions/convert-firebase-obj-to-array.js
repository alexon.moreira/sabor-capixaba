export const convertFirebaseObjToArray = snapshot => {
    let registros = []
    snapshot.forEach(i => {
        let registro = i.val()
        registro.key = i.key
        registros.push(registro)
    });
    return registros
};