export function capitalizePalavra(palavra) {
    let a = palavra.slice(0,1).toUpperCase();
    let b = palavra.slice(1, palavra.length).toLowerCase();
    palavra = (a+b); 
    return palavra.trim();
}