export const sync = async (result, callback) => {
    try {
      const dados = await result
      return callback(dados) 
    } 
    catch(error) {
        return callback(error)
    }
  }