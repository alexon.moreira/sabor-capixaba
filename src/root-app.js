import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {Provider} from "react-redux";
import registerServiceWorker from './registerServiceWorker';
import Store from './redux/store';
import App from './components/app';

class RootApp extends Component {
    render() {
      return (
        <Provider store={Store}>
          <App/>
        </Provider>
      );
    }
  }

export default RootApp

ReactDOM.render(<RootApp />, document.getElementById('root'));
registerServiceWorker();