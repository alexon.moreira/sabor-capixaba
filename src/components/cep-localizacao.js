import React, {Component} from 'react';
import {View, TextInput} from 'react-native';
import {Redirect} from 'react-router-native';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {Creators as Actions} from '../redux/actions';
import {Button3D, BackgroundImage} from '../default-components/index';
import {trim} from '../default-functions/trim.js';
import {sonumeros} from '../default-functions/sonumeros.js';
import '../css/cep-localizacao.css';
//const soap2 = require('soap-everywhere');
//const soap = require('soap');

class CepLocalizacao extends Component {    
    constructor(props){
        super(props);
        this.state = {
            stateCep:'',
        }
    }

    fnValidarInputCep = (text) => {
        if (/^\d+$/.test(text) || text.length === 0) {
            this.setState({stateCep:text});
        }
    }

    /* 
    // SOAP CORREIOS BRASIL - o soap ainda não funciona 
    fnCepCorreios = () => {
        const url = 'https://apphom.correios.com.br/SigepMasterJPA/AtendeClienteService/AtendeCliente?wsdl';        
        const args = {cep: '29160114'};
        soap2.createClient(url, function(err, client) {
            client.consultaCEP(args, function(err, result) {
                console.log(result);
            });
        });
    }
    */    
    
    fnViaCep = async () => {        
        const {actionEnderecoAtivo} = this.props;
        const {propsEnderecoAtivo_ENDNUM, propsEnderecoAtivo_ENDATIVO} = this.props;
        try {                
                const res = await fetch(`https://viacep.com.br/ws/${this.state.stateCep}/json/`, {mode: 'cors'});
                const end = await res.json();  
                console.log(end);
                let cep = sonumeros(trim(end.cep));
                if(cep !== '' || cep !== null || cep !== undefined) {
                    let cep = sonumeros(trim(end.cep));
                    let logradouro = end.logradouro
                    let endNum = propsEnderecoAtivo_ENDNUM;
                    let bairro = end.bairro
                    let municipio = end.localidade
                    let estado = end.uf
                    let pais = 'Brasil';
                    actionEnderecoAtivo({redirect:true, endAtivo:propsEnderecoAtivo_ENDATIVO, cep:cep, logradouro:logradouro, endNum:endNum, bairro:bairro, municipio:municipio, estado:estado, pais:pais})
                }
            }
        catch(erro){
            console.log('Não foi possivel encontrar o Cep!', erro)
        }
    }

    fnRedirectConfirmaEndereco = () => {
        return (<Redirect to={{pathname:'/confirma-endereco'}}/>);
    }

    fnRenderCep() {
        return (
            <View className='container-cep'>
                <BackgroundImage backgroundImage='img2.jpg' />
                <TextInput className='input-cep' style={{zIndex:1, maxWidth:200, padding:10, fontSize:'1.3em', fontFamily:'Arial Black', backgroundColor:'white'}} value={this.state.stateCep} onChangeText={this.fnValidarInputCep} placeholder='CEP' autoFocus={true} />
                <Button3D icon='language' onClick={this.fnViaCep}>Buscar Cep!</Button3D>
            </View>
        )
    }

    render() {
        const {propsEnderecoAtivo_REDIRECT} = this.props.reducerEnderecoAtivo
        if (propsEnderecoAtivo_REDIRECT === true) {
            return (this.fnRedirectConfirmaEndereco());
        }
        else {
            return (this.fnRenderCep());
        }        
    }
}

const mapStateToProps = state => ({...state});
const mapDispatchToProps = dispatch => bindActionCreators(Actions, dispatch);
export default connect(mapStateToProps, mapDispatchToProps)(CepLocalizacao);