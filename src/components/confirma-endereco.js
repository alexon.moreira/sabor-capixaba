import React, {Component} from 'react';
import {View, AsyncStorage} from 'react-native';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {Creators as Actions} from '../redux/actions';
import {Button3D, InputBox} from '../default-components/index';
import '../css/confirma-endereco.css';

class ConfirmaEndereco extends Component {
    constructor(props){
        super(props);
        const {propsEnderecoAtivo_CEP, propsEnderecoAtivo_ENDATIVO, propsEnderecoAtivo_REDIRECT, propsEnderecoAtivo_LOGRADOURO, propsEnderecoAtivo_ENDNUM, propsEnderecoAtivo_BAIRRO, propsEnderecoAtivo_MUNICIPIO, propsEnderecoAtivo_ESTADO, propsEnderecoAtivo_PAIS} = this.props.reducerEnderecoAtivo
        this.state = {            
            cep:        propsEnderecoAtivo_CEP,
            logradouro: propsEnderecoAtivo_LOGRADOURO,
            endNum:     propsEnderecoAtivo_ENDNUM,
            bairro:     propsEnderecoAtivo_BAIRRO,
            municipio:  propsEnderecoAtivo_MUNICIPIO,
            estado:     propsEnderecoAtivo_ESTADO,
            pais:       propsEnderecoAtivo_PAIS,
            endAtivo:   propsEnderecoAtivo_ENDATIVO,
            redirect:   propsEnderecoAtivo_REDIRECT,
        }
    }

    componentDidMount() {
        this.props.actionEnderecoAtivo({...this.state, redirect:false});
    }

    fnOnChange = (e) => {
        let inputName = e.target.name;
        let inputValue = e.target.value;
        this.setState({[inputName]:inputValue});
    }

    fnConfirmaEndereco = () => {
        this.fnAsyncStorageSetItem({...this.state, redirect:false, endAtivo:true});
        this.props.actionEnderecoAtivo({...this.state, redirect:false, endAtivo:true});
    }

    fnAsyncStorageSetItem(obj) {
        AsyncStorage.setItem('AS_ENDERECO_ATIVO', JSON.stringify(obj));
    }    

    render() {        
        const {cep, logradouro, endNum, bairro, municipio, estado, pais} = this.state;
        return (            
            <View className='container-confirma-endereco'>
                {/* <BackgroundImage backgroundImage='img5.jpg' /> */}
                <View className='form'>                    
                    <InputBox widthBox="30%" label='Cep'        name='cep'          value={cep}        onChange={this.fnOnChange} placeholder='CEP' type='numeric' />
                    <InputBox widthBox="60%" label='Logradouro' name='logradouro'   value={logradouro} onChange={this.fnOnChange} placeholder='Endereço' />
                    <InputBox widthBox="30%" label='Número'     name='endNum'       value={endNum}     onChange={this.fnOnChange} placeholder='Número' autoFocus={true} />
                    <InputBox widthBox="60%" label='Bairro'     name='bairro'       value={bairro}     onChange={this.fnOnChange} placeholder='Bairro' />
                    <InputBox widthBox="60%" label='Município'  name='municipio'    value={municipio}  onChange={this.fnOnChange} placeholder='Município' />
                    <InputBox widthBox="40%" label='Estado'     name='estado'       value={estado}     onChange={this.fnOnChange} placeholder='Estado' />
                    <InputBox widthBox="40%" label='Pais'       name='pais'         value={pais}       onChange={this.fnOnChange} placeholder='País' />
                </View>                
                <View className='box-buttons'>                    
                    <Button3D icon='map' onClick={this.fnConfirmaEndereco}>Confirmar!</Button3D>
                </View>
            </View>
        )    
    }
}

const mapStateToProps = state => ({...state});
const mapDispatchToProps = dispatch => bindActionCreators(Actions, dispatch);
export default connect(mapStateToProps, mapDispatchToProps)(ConfirmaEndereco);