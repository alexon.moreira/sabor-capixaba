import React, {Component} from 'react';
import {View, Text, AsyncStorage} from 'react-native';
import Icon from 'react-native-vector-icons/dist/MaterialIcons';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {Creators as Actions} from '../redux/actions';
import {Link} from '../default-components/index';
import {Button} from '../default-components/index.js';
import '../css/endereco-ativo.css';

class EnderecoAtivo extends Component {
    
    componentDidMount = async () => {
        const asyncStorage = await AsyncStorage.getItem('AS_ENDERECO_ATIVO');
        const AS_ENDERECO_ATIVO = JSON.parse(asyncStorage) || [];
        this.props.actionEnderecoAtivo(AS_ENDERECO_ATIVO);
    };

    fnAsyncStorageRemoveItem = () => {
        AsyncStorage.removeItem('AS_ENDERECO_ATIVO');
        this.props.actionEnderecoAtivo({...this.state, endAtivo:false});
    }    

    fnViewBarraEndereco(){        
        const {propsEnderecoAtivo_LOGRADOURO, propsEnderecoAtivo_ENDNUM, propsEnderecoAtivo_BAIRRO, propsEnderecoAtivo_MUNICIPIO, propsEnderecoAtivo_ESTADO, propsEnderecoAtivo_PAIS, propsEnderecoAtivo_CEP}  =  this.props.reducerEnderecoAtivo;
        return (
            <View className='container-endereco-ativo'>
                <View className='box-endereco-ativo'>
                    <Button onClick={this.fnAsyncStorageRemoveItem}><Icon name='cancel' size={30}/></Button>
                    <Text className='texto-endereco-ativo' adjustsFontSizeToFit numberOfLines={2}>
                        {propsEnderecoAtivo_LOGRADOURO} n&#186; {propsEnderecoAtivo_ENDNUM} - {propsEnderecoAtivo_BAIRRO} - {propsEnderecoAtivo_MUNICIPIO}/{propsEnderecoAtivo_ESTADO} - {propsEnderecoAtivo_PAIS} - Cep: {propsEnderecoAtivo_CEP}</Text>
                    <Link className='link-endereco-ativo' onClick={this.toggleMenu} to="/cep-localizacao"><Icon name="room" size={30}/></Link>
                </View>
            </View>
        )
    }

    render() {
        const {propsEnderecoAtivo_ENDATIVO}  =  this.props.reducerEnderecoAtivo;
        if (propsEnderecoAtivo_ENDATIVO === true) {
            return (this.fnViewBarraEndereco());
        }
        else {
            return (<View></View>);
        }
    };
}

const mapStateToProps = state => ({...state});
const mapDispatchToProps = dispatch => bindActionCreators(Actions, dispatch);
export default connect(mapStateToProps, mapDispatchToProps)(EnderecoAtivo);