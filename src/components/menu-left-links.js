import React, {Component} from 'react'
import {View, Text} from 'react-native';
//import Icon from 'react-native-vector-icons/dist/MaterialIcons';
import {Link} from '../default-components/index';
import '../css/menu-left-links.css';

const LINKS = [    
    {path:'/', descricaoLink:'Home'},
    {path:'/about', descricaoLink:'Sobre'},
    {path:'/contact', descricaoLink:'Contato'},
    {path:'/cep-localizacao', descricaoLink:'Localizar CEP'},
    {path:'/confirma-endereco', descricaoLink:'Confirmar Endereço'},
    {path:'/categoria', descricaoLink:'Adicionar Categoria'}
]

class MenuLeftLinks extends Component {    

    fnLinks() {
        return (LINKS.map(
            (i, index) => (
                <Text key={index} adjustsFontSizeToFit numberOfLines={1}>
                    <Link className="menu-left-links" to={i.path}>{i.descricaoLink}</Link>
                </Text>            
            )
        ))
    };
    
    render() {
        return (
            <View>
                {this.fnLinks()}
            </View>
        )
    }    
}
export default MenuLeftLinks