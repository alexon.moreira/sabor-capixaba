import React, {Component} from 'react';
import {View, PanResponder, Animated} from 'react-native';
import {bindActionCreators} from "redux";
import {Creators as Actions} from '../redux/actions';
import {connect} from "react-redux";

/** Componenet Default */
//import BackgroundImage from '../default-components/background-image';
import {isWeb} from '../default-components/helpers';
import BackgroundVideo from '../default-components/background-video';

/** Componenet da App */
import MenuLeft from './menu-left';
import Header from './header';
import EnderecoAtivo from './endereco-ativo';
import Rotas from './rotas';
import Footer from './footer';
import Login from './login';

/**** DB ****/
import {firebase} from '../db/firebase';
import '../css/app.css';

class App extends Component  {

  constructor(props){
    super(props)    
    console.log(firebase);
    this.state = {
      pan: new Animated.ValueXY()
    }    
  }

    componentWillMount() {
        this.fnPanResponderMenuLeft();
    }

    fnPanResponderMenuLeft = () => {        
        this.panMenuLeft = PanResponder.create({
            onMoveShouldSetResponderCapture:() => true,
            onMoveShouldSetPanResponderCapture:() => true,
            
            onPanResponderGrant:(evt, gestureState) => {
                // Valores Iniciais
                this.state.pan.setOffset({x:this.state.pan.x._value});
                this.state.pan.setValue({x:0})
            },
            
            onPanResponderMove: (evt, gestureState) => {
              //console.log('EVENTOS -> ', evt);
              //console.log('GESTURE STATE -> ', gestureState);
              //console.log('Valor de X -> ', this.state.pan.x);
              Animated.event([null, {dx:this.state.pan.x}])(evt, gestureState)
            },

            onPanResponderRelease:(evt, {vx}) => {
              this.state.pan.flattenOffset();
            }
        });        
    }    

    fnViewApp = () => {
      console.log('Alexon Passou por aqui')        
      const {propsMenuLeft_WIDTH}  =  this.props.reducerMenuLeft;
      let {pan} = this.state;    
      let [translateX] = [pan.x];    
      let styleMoveMenuLeft = {transform:[{translateX}]};            
      return (
        <Router>
          <View className='app-container'>
            {/*
              <View className='app-login-container'>
                  <Login />
              </View>
            */}
              {/* <BackgroundImage backgroundImage='img4.jpg' /> */}
              <BackgroundVideo />
              <Animated.View className='app-menu-left-container' {...this.panMenuLeft.panHandlers} style={{...styleMoveMenuLeft, width:propsMenuLeft_WIDTH}}>
                  <MenuLeft />
              </Animated.View>
          
              <View className='app-content-container'>
                  <View className='app-content-header'>
                      <Header />
                  </View>
            
                  <View className='app-content'>
                      <EnderecoAtivo />
                      <Rotas />
                  </View>
            
                  <View className='app-content-footer'>
                      <Footer />
                  </View>
              </View> 
          </View>
        </Router>
      )
    }

  render() {
    return (this.fnViewApp());
  }
}

let Router;
  if(isWeb) {
      Router = require('react-router-dom').BrowserRouter    
  }
  else {
      Router = require('react-router-native').NativeRouter    
  }
  
  if(isWeb) {
    const iconFont = require('react-native-vector-icons/Fonts/MaterialIcons.ttf');
    const iconFontStyles = `@font-face {src: url(${iconFont}); font-family: "Material Icons"}`;
  
    const style = document.createElement('style');
    style.type = 'text/css';
    
    if(style.styleSheet) {
      style.styleSheet.cssText = iconFontStyles
    } 
    else {
      style.appendChild(document.createTextNode(iconFontStyles))
    }
    document.head.appendChild(style);
  }

const mapStateToProps = state => ({...state});
const mapDispatchToProps = dispatch => bindActionCreators(Actions, dispatch);
export default connect(mapStateToProps, mapDispatchToProps)(App);