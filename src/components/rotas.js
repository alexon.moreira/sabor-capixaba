import React, {Component} from 'react';
import {Route} from 'react-router-native';
import Home from './home';
import About from './about';
import Contact from './contact';
import CepLocalizacao from './cep-localizacao';
import ConfirmaEndereco from './confirma-endereco';
import Categoria from './categoria';

const ROTAS = [
    {path:'/', exact:true, renderComponent:() => <Home />},
    {path:'/home', renderComponent:() => <Home />},
    {path:'/about', renderComponent:() => <About />},
    {path:'/contact', renderComponent:() => <Contact />},
    {path:'/cep-localizacao', renderComponent:() => <CepLocalizacao />},
    {path:'/confirma-endereco', renderComponent:() => <ConfirmaEndereco />},
    {path:'/categoria', renderComponent:() => <Categoria />}
]

class Rotas extends Component {
    fnRotas() {
        return (
            ROTAS.map((i, index) => (
                <Route key={index} path={i.path} exact={i.exact} component={i.renderComponent} />
            ))        
        )   
    };

    render() {
        return (this.fnRotas())
    }    
}
export default Rotas