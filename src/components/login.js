import React, {Component} from 'react';
import {View, Text, Image} from 'react-native';
import {Button3D, InputBox} from '../default-components/index';
import '../css/login.css';

class Login extends Component {
  constructor(props) {
    super(props)
    this.state = {
      email:'',
      confirmaEmail:'',
      senha:'', 
      confirmaSenha:'',
      isRenderLogin:true,
      buttonOFF:{pointerEvents:'none', opacity:.5}
    }
  }

  fnOnChange = (e) => {
    let inputName = e.target.name;
    let inputValue = e.target.value;
    this.setState({[inputName]:inputValue});
  }

  fnConfirmaLogin = () => {
    console.log('STATE LOGIN:', {...this.state});
  }

  fnComparaEmail = () => {
    let confirmaEmail = (this.state.email === this.state.confirmaEmail);
    confirmaEmail? this.setState({buttonOFF:{pointerEvents:'all', opacity:1}}) : this.setState({...this.state.buttonOFF})
  }

  fnCadastroLogin = () => {
    const {email, confirmaEmail, senha, confirmaSenha} = this.state;
    return (      
        <View className='cadastro-usuario-form'>
            <Text className='login-label'>Email:</Text>
            <InputBox widthBox="100%" label='Email' name='email' value={email} onChange={this.fnOnChange} />
            <Text className='login-label'>Confirma Email:</Text>
            <InputBox widthBox="100%" label='Confirma Email' name='confirmaEmail' value={confirmaEmail} onChange={this.fnOnChange} />
            <Text className='login-label' style={{width:'48%'}}>Senha:</Text>
            <Text className='login-label' style={{width:'48%'}}>Confirma Senha:</Text>
            <InputBox widthBox="48.5%" secureTextEntry={true} label='Senha' name='senha' value={senha} onChange={this.fnOnChange} />
            <InputBox widthBox="48.5%" secureTextEntry={true} label='Confirma Senha' name='confirmaSenha' value={confirmaSenha} onChange={this.fnOnChange} />
            <View className='login-buttons' pointerEvents={this.state.buttonOFF.pointerEvents}>
                <Button3D icon='map' style={{opacity:this.state.buttonOFF.opacity}} onClick={this.fnConfirmaLogin}>CONFIRMA</Button3D>
            </View>                    
        </View>
    )
  }

  fnAcessoLogin = () => {    
    const {email, senha} = this.state;
    return (      
        <View className='login-usuario'>
            <Text className='login-label'>Email:</Text>
            <InputBox widthBox="100%" label='Email' name='email' value={email} onChange={this.fnOnChange} />
            <Text className='login-label' style={{width:'100%'}}>Senha:</Text>
            <InputBox widthBox="100%" secureTextEntry={true} label='Senha' name='senha' value={senha} onChange={this.fnOnChange} />
            <View className='login-buttons' pointerEvents='none'>
                <Button3D icon='map' style={{opacity:.5}} onClick={this.fnConfirmaLogin}>ACESSAR</Button3D>
            </View>                    
        </View>
    )
  }

  fnRender = () => {
    this.setState({isRenderLogin:!this.state.isRenderLogin});
  }
 
  render() {
    console.log(this.state.isRenderLogin);
    const render = this.state.isRenderLogin;
    return (
        <View className='login-container'>
            <Image className='logo2' style={[{resizeMode:'cover', width:211, height:62}]} source={require('../images/logo-sabocapixaba-login.svg')}></Image>
            {render? this.fnAcessoLogin() : this.fnCadastroLogin()}
            <View className='login-buttons'>
                <Button3D icon='verified-user' style={{backgroundColor:'tomato'}} onClick={this.fnRender}>LOGIN</Button3D>
                <Button3D icon='add' style={{backgroundColor:'green'}} onClick={this.fnRender}>CADASTRO</Button3D>
            </View>                                
        </View>
    )
  };
} 

export default Login;