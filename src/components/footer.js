import React, {Component} from 'react';
import {View, Text} from 'react-native';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {Creators as Actions} from '../redux/actions';
import '../css/footer.css';

class Footer extends Component {

  render() {
    return (
      <View className='footer-container'>
        <View className='footer-box'>
            <Text>Footer</Text>
        </View>
      </View>
    )    
  }
}

const mapStateToProps = state => ({...state});
const mapDispatchToProps = dispatch => bindActionCreators(Actions, dispatch);
export default connect(mapStateToProps, mapDispatchToProps)(Footer);