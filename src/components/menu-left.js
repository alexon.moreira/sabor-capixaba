import React, {Component} from 'react';
import {View, Image} from 'react-native';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {Creators as Actions} from '../redux/actions';
import MenuLeftLinks from './menu-left-links';
import {Button} from '../default-components/index.js';
import '../css/menu-left.css';

class MenuLeft extends Component {

    fnCloseMenuleft = () => {
        this.props.actionMenuLeft({width:0});
    }

    fnViewMenuLeft(){
        const {propsMenuLeft_WIDTH}  =  this.props.reducerMenuLeft;
        return (            
           <View className='menu-left' style={[{width:propsMenuLeft_WIDTH}]}>
                    <View className='close-menu-left'>
                        <Button onClick={this.fnCloseMenuleft}>X</Button>
                    </View>                    
                    <View className='menu-left-header' style={[{width:'100%'}]}>
                        <Image className='logo2' style={[{resizeMode:'cover', width:180, height:55}]}  source={require('../images/saborcapixaba/logo2.png')}></Image>
                    </View>
                    <View className='links-container' style={[{width:'100%'}]}>
                        <MenuLeftLinks />
                    </View>           
           </View>       
        )
    }

    render() {
        return (this.fnViewMenuLeft());
    };
}

const mapStateToProps = state => ({...state});
const mapDispatchToProps = dispatch => bindActionCreators(Actions, dispatch);
export default connect(mapStateToProps, mapDispatchToProps)(MenuLeft);