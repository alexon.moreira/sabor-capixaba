import React, {Component} from 'react';
import {View, Text} from 'react-native';
import GeoLocalizacao from './geo-localizacao';
import '../css/home.css';

class Home extends Component {
  
  render() {
    return ( 
      <View className='home-container'>
        <View className='home-box'>        
            <View className='home-textos'>
              <Text className='home-titulo'>O melhor do Sabor Capixaba!</Text>
              <Text className='home-sub-titulo'>Simplificando seu pedido...</Text>
            </View>
            <View className='home-geo-localizacao'>
              <GeoLocalizacao></GeoLocalizacao>
            </View>
        </View>        
      </View>
    )    
  };
} 

export default Home;