import React, {Component} from 'react';
import {View, Text, Image, Dimensions} from 'react-native';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {Creators as Actions} from '../redux/actions';
import Icon from 'react-native-vector-icons/dist/MaterialIcons';
import {Button, Link} from '../default-components/index.js';
import '../css/header.css';

class Header extends Component {
  constructor(props) {
    super(props)
    this.state = {
      stateUsuarioLogado:false,
     
      /* State CSS Button 3D */
      stateOpacity:1,
      stateColor:'#FFF',
      statePointerEvents:'auto',
      stateCursor:'pointer',
      stateFontSize:'1.3em'
    }
  }

  fnLogo() {
    console.log('logo');
  }

  fnSetStateUsuarioLogado = () => {
    this.setState({stateUsuarioLogado:!this.state.stateUsuarioLogado})
  }

  fnRenderUsuarioLogado = () => {
    try {        
        if(this.state.stateUsuarioLogado) {          
          return (            
            <Button className='button-usuario'>
              <Icon name="person" size={40} color="white" />
            </Button>
          )
        }
        else {
          return <Text></Text>
        }   
    }
    catch(error){
      console.log('usuario logado error!');
    }    
  }
  
fnAtivarMenuleft = () => {
    const windowWidth = Dimensions.get('window').width;    
    let val = 0;  
    if(windowWidth > 400 ? val=290 : val='100%');
    if(this.props.reducerMenuLeft.propsMenuLeft_WIDTH === 0? val:val=0);
    //if((windowWidth < 400) && (val >=300) || val >= '85%')? displayImageLogo='none': displayImageLogo='block');
    this.props.actionMenuLeft({width:val});
}

  render() {
    return (
      <View className='header-container'>
        <View className='header-box'>
        
          <View className='menu'>
            <Button className='button-menu' onClick={this.fnAtivarMenuleft} style={{backgroundColor:'transparent', width:50, height:'100%'}}>
              <Icon name="menu" size={30} color="white" />            
            </Button>
          </View>

          <View className='logo'>
            <Link className='link-home' onClick={this.fnLogo} to="/home">             
              <Image className='img-saborcapixaba' resizeMode='stretch' source={require('../images/logo-166x48.png')} style={{display:'none'}}/>
            </Link>
          </View>

          <View className='usuario'>
            {this.fnRenderUsuarioLogado()}
          </View>

        </View>
      </View>
    )    
  }
}

const mapStateToProps = state => ({...state});
const mapDispatchToProps = dispatch => bindActionCreators(Actions, dispatch);
export default connect(mapStateToProps, mapDispatchToProps)(Header);