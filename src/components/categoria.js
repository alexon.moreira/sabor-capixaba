import React, {Component} from 'react';
import {View} from 'react-native';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {Creators as Actions} from '../redux/actions';
import {Button3D, InputBox} from '../default-components/index';
import '../css/categoria.css';

class Categoria extends Component {
    constructor(props){
        super(props);
        this.state = {            
            categoriaID:'',
            categoriaDescricao:''
        }
    }

    fnOnChange = (e) => {
        let inputName = e.target.name;
        let inputValue = e.target.value;
        this.setState({[inputName]:inputValue});
    }

   fnConfirma = () => {
       console.log('Clicou aqui');
   }

    render() {        
        const {categoriaDescricao} = this.state;
        return (            
            <View className='container-categoria'>        
                <View className='form'>                    
                    <InputBox widthBox="100%" label='Descricao' name='categoria' value={categoriaDescricao} onChange={this.fnOnChange} placeholder='Descricao' />
                </View>                
                <View className='container-buttons-categoria'>                    
                    <Button3D icon='add' onClick={this.fnConfirmaEndereco}>Adcionar Categoria</Button3D>
                </View>
            </View>
        )    
    }
}

const mapStateToProps = state => ({...state});
const mapDispatchToProps = dispatch => bindActionCreators(Actions, dispatch);
export default connect(mapStateToProps, mapDispatchToProps)(Categoria);