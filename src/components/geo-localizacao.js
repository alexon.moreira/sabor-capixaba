import React, {Component} from 'react';
import {View} from 'react-native';
import {Redirect} from 'react-router-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {Creators as Actions} from '../redux/actions';
import {Button3D, LinkButton} from '../default-components/index.js';
import {trim} from '../default-functions/trim.js';
import {sonumeros} from '../default-functions/sonumeros.js';
import '../css/geo-localizacao.css';

class GeoLocalizacao extends Component {
  
    fnEndereco = () => {
        let lat = 0;
        let lon = 0;        
        if ("geolocation" in navigator) {
               navigator.geolocation.getCurrentPosition((position) => {
               lat = position.coords.latitude;
               lon =  position.coords.longitude;
               console.log(lat,lon);
               let url = `https://maps.googleapis.com/maps/api/geocode/json?latlng=${lat},${lon}&sensor=false&key=AIzaSyBI5It_TMcEeX4vkktA_Vdesa6_X-iDyYU`;
               setTimeout(() => {this.fnFetchEndereco(url)}, 200);
            })            
        }
    }

    fnFetchEndereco = (url) => {
        fetch(url)
            .then(res => {
                console.log(res.status)
                console.log(res.ok)
                if (res.status === 200 || res.status === 400) {
                    return res.json()
                } else {
                    throw new Error(res.text())
                }        
            })
            .then(res => {
                const {actionEnderecoAtivo} = this.props;        
                const {propsEnderecoAtivo_ENDATIVO, propsEnderecoAtivo_ENDNUM}  =  this.props.reducerEnderecoAtivo;
                let end = (res.results[0].formatted_address).split(',');
                let cep = sonumeros(trim(end[3]));
                console.log(cep);
                if(cep !== '' || cep !== null || cep !== undefined) {             
                    let cep = sonumeros(trim(end[3]));
                    let logradouro = end[0];
                    let endNum = propsEnderecoAtivo_ENDNUM;
                    let bairro = trim(end[1].split('-')[1]);                 
                    let municipio = trim(end[2].split('-')[0]);
                    let estado = trim(end[2].split('-')[1]);
                    let pais = trim(end[4]);                
                    actionEnderecoAtivo({redirect:true, endAtivo:propsEnderecoAtivo_ENDATIVO, cep:cep, logradouro:logradouro, endNum:endNum, bairro:bairro, municipio:municipio, estado:estado, pais:pais});
                }
            })
            .catch((error) => {
                console.log(error.message)
            })
    }    

    fnRedirectConfirmaEndereco = () => {
        return (<Redirect to={{pathname:'/confirma-endereco'}}/>);
    }    

    fnViewGeoLocalizacao = () => {
        return (
            <View className='container-geo'>
                <Button3D icon='map' onClick={this.fnEndereco}>Meu Endereço Google Maps</Button3D>
                <LinkButton icon='language' to="/cep-localizacao" onClick={this.fnComponentCep}>Meu Endereço CEP</LinkButton>
            </View>
        )
    }

    render() {
        const {propsEnderecoAtivo_REDIRECT} = this.props.reducerEnderecoAtivo
        if (propsEnderecoAtivo_REDIRECT === true) {
            return (this.fnRedirectConfirmaEndereco());
        }
        else {
            return (this.fnViewGeoLocalizacao());
        }        
    }
}

const mapStateToProps = state => ({...state});
const mapDispatchToProps = dispatch => bindActionCreators(Actions, dispatch);
export default connect(mapStateToProps, mapDispatchToProps)(GeoLocalizacao);

/** OUTROAS DICAS GEO LOCATION */
    /* LOCALIZAR ENDEREÇO FORMA LITERAL - EX: RUA 14, 81, CARAPINA GRANDE, SERRA, ES */
    //const res = await fetch(`https://maps.google.com/maps/api/geocode/json?address=rua 15&carapina+grande&serra&es&&region=BR`) 
            
    /* INTERESSANTE PARAMENTROS ORIGEN E DESTINOS CEPS - CALCULAR DISTÂNCIAS */
    //http://maps.googleapis.com/maps/api/distancematrix/json?origins=29160114&destinations=29160114&region=BR&sensor=false


//<Button3D opacity={this.state.stateOpacity} color={this.state.stateColor} pointerEvents={this.state.statePointerEvents} cursor={this.state.stateCursor} backgroundColor={this.state.stateBackgroundColor} onClick={this.fnSetRedirect}>