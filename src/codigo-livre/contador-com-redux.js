import React, {Component} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from "redux";
import {Creators as Actions} from '../redux/actions';
import {Button} from '../default-components/index';

class ContadorComRedux extends Component {    
    
    fnInc = () => {
        this.props.actionContadorInc();
    }

    fnDec = () => {
        this.props.actionContadorDec();
    }

    render() {
        const {propsContador_NUM} = this.props.reducerContadorInc
        return (
            <View style={styles.containerGlobal}>
                <View style={styles.containerSemRedux}>
                    <Text style={styles.texto1} adjustsFontSizeToFit numberOfLines={1}>Estudo React com Redux</Text>
                    <View style={styles.containerContador}>
                        <Button onClick={this.fnInc} style={styles.box}>
                            <Text style={styles.texto2}>+</Text>
                        </Button>                        
                        <Text style={[styles.boxContador, styles.texto2]}>{propsContador_NUM}</Text>
                        <Button onClick={this.fnDec} style={styles.box}>
                            <Text style={styles.texto2}>-</Text>
                        </Button>
                    </View>
                </View>                
            </View>
        )
    }
}

const styles = StyleSheet.create({
    containerGlobal:  {display:'flex', flex:1, flexDirection:'row', alignItems:'center', justifyContent:'center'},
    containerSemRedux:  {display:'flex', flexDirection:'column', alignItems:'center', justifyContent:'center'},
    containerContador:  {display:'flex', flex:1, flexDirection:'row', alignItems:'center', justifyContent:'center'},    
    box: {backgroundColor:'blue', display:'flex', width:50, height:50, alignItems:'center', justifyContent:'center'},
    boxContador: {padding:10, backgroundColor:'black', display:'flex', width:50, height:50, alignItems:'center', justifyContent:'center'},
    texto1:{display:'flex', fontSize:'150%', color:'white', width:'100%', height:45, alignItems:'center', justifyContent:'center'},
    texto2:{display:'flex', fontSize:'2.5em', color:'white', height:45, alignItems:'center', justifyContent:'center'},
})

const mapStateToProps = state => ({...state});
const mapDispatchToProps = dispatch => bindActionCreators(Actions, dispatch);
export default connect(mapStateToProps, mapDispatchToProps)(ContadorComRedux);
