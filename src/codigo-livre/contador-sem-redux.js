import React, {Component} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {Button} from '../default-components/index';

class ContadorSemRedux extends Component {
    state = {stateContador:0}
    fnInc = () => {
        this.setState({stateContador: this.state.stateContador + 1})
    }

    fnDec = () => {
        this.setState({stateContador:this.state.stateContador - 1})
    }

    render() {
        const {stateContador} = this.state
        return (
            <View style={styles.containerGlobal}>
                <View style={styles.containerSemRedux}>
                    <Text style={styles.texto1} adjustsFontSizeToFit numberOfLines={1}>Estudo React sem Redux</Text>
                    <View style={styles.containerContador}>
                        <Button onClick={this.fnInc} style={styles.box}>
                            <Text style={styles.texto2}>+</Text>
                        </Button>
                        
                        <Text style={[styles.boxContador, styles.texto2]}>{stateContador}</Text>

                        <Button onClick={this.fnDec} style={styles.box}>
                            <Text style={styles.texto2}>-</Text>
                        </Button>
                    </View>
                </View>                
            </View>
        )
    }
}

const styles = StyleSheet.create({
    containerGlobal:  {display:'flex', flex:1, flexDirection:'row', alignItems:'center', justifyContent:'center'},
    containerSemRedux:  {display:'flex', flexDirection:'column', alignItems:'center', justifyContent:'center'},
    containerContador:  {display:'flex', flex:1, flexDirection:'row', alignItems:'center', justifyContent:'center'},    
    box: {backgroundColor:'blue', display:'flex', width:50, height:50, alignItems:'center', justifyContent:'center'},
    boxContador: {padding:10, backgroundColor:'black', display:'flex', width:50, height:50, alignItems:'center', justifyContent:'center'},
    texto1:{display:'flex', fontSize:'150%', color:'white', width:'100%', height:45, alignItems:'center', justifyContent:'center'},
    texto2:{display:'flex', fontSize:'2.5em', color:'white', height:45, alignItems:'center', justifyContent:'center'},
})

export default ContadorSemRedux;